import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Products from '@/components/Products'
import Customers from '@/components/customers'
import ItemsList from '@/components/Items'
import ItemCreate from '@/components/Items/create'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    },
    {
      path: '/customers',
      name: 'Customers',
      component: Customers
    },
    {
      path: '/items',
      name: 'ItemsList',
      component: ItemsList
    },
    {
      path: '/item-create',
      name: 'ItemCreate',
      component: ItemCreate
    }
  ]
})
