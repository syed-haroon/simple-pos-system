import Vue from 'vue'
import * as firebase from 'firebase'

import {
  Vuetify,
  VApp,
  VDataTable,
  VDivider,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VCard,
  VToolbar,
  VTextField,
  VSelect,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

import App from './App'
import router from './router'

Vue.use(Vuetify, {
  components: {
    VApp,
    VDataTable,
    VDivider,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VCard,
    VToolbar,
    VTextField,
    VSelect,
    transitions
  },
  theme: {
    primary: '#0083c0',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyCziAyVFOfb5dELdmQblGoXjBK_JprPeRY',
      authDomain: 'simple-pos-5eba3.firebaseapp.com',
      databaseURL: 'https://simple-pos-5eba3.firebaseio.com',
      projectId: 'simple-pos-5eba3',
      storageBucket: 'simple-pos-5eba3.appspot.com'
    })

    // firebase.auth().createUserWithEmailAndPassword('seek.syed@gmail.com', 'yahoooo786')
    //   .then((data) => {
    //     console.log(data)
    //   })

    firebase.auth().signInWithEmailAndPassword('seek.syed@gmail.com', 'yahoooo786')
      .then((data) => {
        console.log(data)
      })
  }
})
